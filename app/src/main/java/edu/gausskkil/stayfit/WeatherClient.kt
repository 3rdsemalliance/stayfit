package edu.gausskkil.stayfit

import android.annotation.SuppressLint
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.State
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableDoubleStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.viewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApi {
    @GET("weather")
    suspend fun getWeather(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double,
        @Query("appid") apiKey: String,
        @Query("units") units: String = "metric",
        @Query("lang") lang: String = "pl"
    ): WeatherResponse
}

data class WeatherResponse(
    val name: String, // Location name
    val main: Main, // Weather data is under "main"
    val weather: List<Weather>
)

data class Main(
    val temp: Double,
    val feels_like: Double,
    val temp_min: Double,
    val temp_max: Double,
    val pressure: Int,
    val humidity: Int
)

data class Weather(
    val description: String,
    val icon: String
)

object RetrofitInstance {
    private val retrofit by lazy {
        Retrofit.Builder()
            .baseUrl("https://api.openweathermap.org/data/2.5/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
    val api: WeatherApi by lazy {
        retrofit.create(WeatherApi::class.java)
    }
}

class WeatherViewModel : ViewModel() {
    private val _weatherData = mutableStateOf<WeatherResponse?>(null)
    val weatherData: State<WeatherResponse?> = _weatherData
    private val _isLoading = mutableStateOf(false)
    val isLoading: State<Boolean> = _isLoading
    fun fetchWeather(lat: Double, lon: Double) {
        viewModelScope.launch {
            _isLoading.value = true
            try {
                val response = RetrofitInstance.api.getWeather(lat, lon, "1e37be0011ded8caf21b3779f0b4e54c")
                _weatherData.value = response
            } catch (e: Exception) {
                e.printStackTrace()
                _weatherData.value = null
            } finally {
                _isLoading.value = false
            }
        }
    }
}

@SuppressLint("MissingPermission")
@Composable
fun WeatherScreen(initialLat: Double, initialLon: Double, database: AppDatabase) {
    val context = LocalContext.current
    val fusedLocationClient: FusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context)
    val viewModel: WeatherViewModel = viewModel()
    var lat by remember { mutableDoubleStateOf(initialLat) }
    var lon by remember { mutableDoubleStateOf(initialLon) }
    val weatherData by viewModel.weatherData
    val isLoading by viewModel.isLoading

    LaunchedEffect(lat, lon) {
        viewModel.fetchWeather(lat, lon)
    }

    Column(
        modifier = Modifier
            .fillMaxWidth()
//            .fillMaxHeight()
            .padding(16.dp),
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Column(horizontalAlignment = Alignment.CenterHorizontally) {
            if (isLoading) {
                Text(text = "Loading weather data...")
            } else {
                weatherData?.let { response ->
                    Column(modifier = Modifier.padding(bottom = 16.dp)) {
                        Text(text = "Location: ${response.name}")
                        Text(text = "Temperature: ${response.main.temp}°C")
                        Text(text = "Feels like: ${response.main.feels_like}°C")
                        Text(text = "Description: ${response.weather[0].description}")
                        Text(text = "Humidity: ${response.main.humidity}%")
                        Text(text = "Pressure: ${response.main.pressure} hPa")
                        Text(text = calculateRecommendation(response.main.pressure.toFloat(), response.main.temp.toFloat(), response.main.humidity.toFloat(), database))
                    }
                } ?: run {
                    Text("No weather data available")
                }
            }
        }
        Button(
            modifier = Modifier
                .padding(top = 8.dp)
                .height(48.dp)
                .width(200.dp),
            onClick = {
                // change lat,lon based on current location
                // WARNING -> NO PERMISSION CHECK (CHECK IS GRANTED IN MAP)
                getCurrentLatLng(fusedLocationClient){ latLng ->
                    lat = latLng.latitude
                    lon = latLng.longitude
                }
                viewModel.fetchWeather(lat, lon)
            }
        ) {
            Text(text = "Refresh weather data")
        }
    }
}