package edu.gausskkil.stayfit

import android.annotation.SuppressLint
import android.location.Location
import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Build
import androidx.compose.material.icons.filled.Clear
import androidx.compose.material.icons.filled.PlayArrow
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.compose.CameraPositionState
import edu.gausskkil.stayfit.ui.theme.SFWhite
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class TimerViewModel(
    val database: AppDatabase,
    val fusedLocationClient: FusedLocationProviderClient,
    val cameraPositionState: CameraPositionState,
    val coroutineScope: CoroutineScope
) : ViewModel() {
    @SuppressLint("SimpleDateFormat")
    private val _timer = MutableStateFlow(0L)
    val timer = _timer.asStateFlow()

    private val _secondaryTimer = MutableStateFlow(0L)
    val secondaryTimer = _timer.asStateFlow()

    private var timerJob: Job? = null
    private var secondaryTimerJob: Job? = null

    private var iter = 0
    private var locations: MutableList<Location> = mutableListOf<Location>()

    fun startTimer() {
        timerJob?.cancel()
        timerJob = viewModelScope.launch {
            while (true) {
                delay(1000)
                _timer.value++
            }
        }
        secondaryTimerJob?.cancel()
        secondaryTimerJob = viewModelScope.launch {
            while (true) {
                delay(5000)
                _secondaryTimer.value++
                getCurrentLatLng(fusedLocationClient) { latLng ->
                    locations.add(createLocation(latLng.latitude, latLng.longitude, "${iter++}C"))
                    coroutineScope.launch {
                        cameraPositionState.animate(CameraUpdateFactory.newLatLng(LatLng(latLng.latitude, latLng.longitude)))
                    }
                }
            }
        }
    }

    fun pauseTimer() {
        timerJob?.cancel()
        secondaryTimerJob?.cancel()
        getCurrentLatLng(fusedLocationClient) { latLng ->
            locations.add(createLocation(latLng.latitude, latLng.longitude, "${iter++}P"))
        }
    }
    fun stopTimer() {
        getCurrentLatLng(fusedLocationClient = fusedLocationClient) { latLng ->
            locations.add(createLocation(latLng.latitude, latLng.longitude, "${iter++}C"))
        }
        viewModelScope.launch {
            try {
                val nextRouteIndex = database.routesDaoInfo().getNextRouteIndex()
                val distance = measurePathsDistance(createPaths(locations))
                val time = _timer.value
                saveRouteInfo(
                    RouteInfo(
                        idTrasy = nextRouteIndex,
                        name = "Trasa $nextRouteIndex",
                        overallTime = time,
                        distance = distance,
                        speed = distance / time * 1000
                    )
                )
                for (location in locations) {
                    saveRouteDetail(
                        RouteDetail(
                            idTrasy = nextRouteIndex,
                            idPunktu = location.provider?.substring(0, location.provider!!.length - 1)?.toInt(),
                            continuation = location.provider?.last(),
                            latitude = location.latitude,
                            longitude = location.longitude
                        )
                    )
                }
                Log.i("NEW ROUTE", "$nextRouteIndex: time: $time, distance: $distance, speed: ${distance/time*1000},num of points: ${locations.size}")
                locations = mutableListOf()
                iter = 0
                _timer.value = 0
                _secondaryTimer.value = 0
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        timerJob?.cancel()
        secondaryTimerJob?.cancel()
    }

    override fun onCleared() {
        super.onCleared()
        timerJob?.cancel()
        secondaryTimerJob?.cancel()
    }

    private suspend fun saveRouteInfo(routeInfo: RouteInfo){
        withContext(Dispatchers.IO){
            database.routesDaoInfo().insertRouteInfo(routeInfo)
        }
    }

    private suspend fun saveRouteDetail(routeDetail: RouteDetail){
        withContext(Dispatchers.IO){
            database.routesDaoDetails().insertRouteDetail(routeDetail)
        }
    }
}

@Composable
fun TimerScreenContent(timerViewModel: TimerViewModel, modifier: Modifier) {
    val timerValue by timerViewModel.timer.collectAsState()
    val secondaryTimerValue by timerViewModel.secondaryTimer.collectAsState()

    TimerScreen(
        timerValue = timerValue,
        onStartClick = { timerViewModel.startTimer() },
        onPauseClick = { timerViewModel.pauseTimer() },
        onStopClick = { timerViewModel.stopTimer() },
        modifier = modifier
    )
}

@Composable
fun TimerScreen(
    timerValue: Long,
    onStartClick: () -> Unit,
    onPauseClick: () -> Unit,
    onStopClick: () -> Unit,
    modifier: Modifier
) {
//    Text("Zmierz swój czas!",
//        modifier = Modifier.fillMaxWidth(), fontSize = 36.sp, textAlign = TextAlign.Center)
    Box(modifier = modifier.background(SFWhite)){
        Text(
            text = timeToText(timerValue),
            modifier = modifier.padding(8.dp),
            fontSize = 24.sp,
            textAlign = TextAlign.Center
        )
        Row(
            modifier = modifier.padding(top = 24.dp),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
        ) {
            IconButton(onClick = onStartClick, modifier = Modifier.padding(8.dp)) {
                Icon(rememberPlayArrow(), contentDescription = "Start")
            }
            IconButton(onClick = onPauseClick, modifier = Modifier.padding(8.dp)) {
                Icon(rememberPause(), contentDescription = "Pause")
            }
            IconButton(onClick = onStopClick, modifier = Modifier.padding(8.dp)) {
                Icon(rememberStop(), contentDescription = "Stop")
            }
        }
    }
}
