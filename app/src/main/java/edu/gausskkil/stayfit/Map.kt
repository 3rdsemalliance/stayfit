package edu.gausskkil.stayfit

import android.Manifest
import android.annotation.SuppressLint
import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Clear
import androidx.compose.material3.Button
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Switch
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.MultiplePermissionsState
import com.google.accompanist.permissions.rememberMultiplePermissionsState
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.maps.android.compose.GoogleMap
import com.google.maps.android.compose.MapProperties
import com.google.maps.android.compose.MapType
import com.google.maps.android.compose.MapUiSettings
import com.google.maps.android.compose.Polyline
import com.google.maps.android.compose.rememberCameraPositionState
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.math.BigDecimal
import java.math.RoundingMode
import kotlin.math.round

@SuppressLint("MissingPermission", "MutableCollectionMutableState")
@Composable
fun MapScreen(database: AppDatabase) {
    val context = LocalContext.current
    val coroutineScope = rememberCoroutineScope()
    val fusedLocationClient: FusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context)
    val cameraPositionState = rememberCameraPositionState {
        getCurrentLatLng(fusedLocationClient = fusedLocationClient) { latLng ->
            position = CameraPosition.fromLatLngZoom(latLng, 5f)
        }
    }

    var uiSettings by remember { mutableStateOf(MapUiSettings(compassEnabled = true)) }
    val mapStyleOptions = remember {
        context.resources.openRawResource(R.raw.night_theme_map).bufferedReader().use { it.readText() }
    }
    var properties by remember { mutableStateOf(MapProperties(mapType = MapType.TERRAIN, isMyLocationEnabled = true, mapStyleOptions = MapStyleOptions(mapStyleOptions))) }
    var selectedRoute by remember { mutableStateOf<Route?>(null) }
    var routes by remember { mutableStateOf(fetchRoutesFromDatabase(database = database)) }
    var showRouteInfo by remember { mutableStateOf(false) }
    var routeInfo by remember { mutableStateOf<RouteInfo?>(null) }

    getCurrentLatLng(fusedLocationClient){ latLng ->
        coroutineScope.launch {
            cameraPositionState.animate(
                update = CameraUpdateFactory.newCameraPosition(
                    CameraPosition(latLng, 15f, 0f, 0f)
                ),
                durationMs = 1000
            )
        }
    }

    Box(modifier = Modifier.fillMaxSize()) {
        GoogleMap(
            modifier = Modifier.fillMaxSize(),
            cameraPositionState = cameraPositionState,
            properties = properties,
            uiSettings = uiSettings,
            onMapClick = { latLng ->
                coroutineScope.launch {
                    cameraPositionState.animate(CameraUpdateFactory.newLatLng(latLng))
                }
            },
            onMyLocationClick = { location ->
                coroutineScope.launch {
                    cameraPositionState.animate(CameraUpdateFactory.newLatLng(LatLng(location.latitude, location.longitude)))
                }
            },
        ) {
            selectedRoute?.let { route ->
                for (points in route.points) {
                    Polyline(
                        points = points,
                        color = Color.Blue,
                        width = 5f,
                        clickable = true,
                        onClick = {
                            coroutineScope.launch {
                                routeInfo = database.routesDaoInfo().fetchRouteInfoFromID(selectedRoute!!.id)
                                showRouteInfo = true
                            }
                        }
                    )
                }
            }
        }
        TimerScreenContent(
            timerViewModel = TimerViewModel(
                database = database,
                fusedLocationClient = fusedLocationClient,
                cameraPositionState = cameraPositionState,
                coroutineScope = coroutineScope
            ),
            modifier = Modifier.align(Alignment.TopCenter)
        )
        Column(modifier = Modifier
            .align(Alignment.BottomCenter)
            .padding(16.dp)) {
            DropdownMenu(routes, selectedRoute, onDelete = { route ->
                coroutineScope.launch {
                    database.routesDaoInfo().deleteRouteInfo(route.id)
                    database.routesDaoDetails().deleteRouteDetails(route.id)
                    routes = fetchRoutesFromDatabase(database = database)
                }
            }) { route ->
                selectedRoute = route
            }
            Switch(
                modifier = Modifier.padding(top = 8.dp),
                checked = uiSettings.zoomControlsEnabled,
                onCheckedChange = {
                    uiSettings = uiSettings.copy(zoomControlsEnabled = it)
                    properties = if (it) {
                        properties.copy(mapType = MapType.TERRAIN)
                    } else {
                        properties.copy(mapType = MapType.SATELLITE)
                    }
                }
            )
            Button(
                onClick = {
                    selectedRoute = null
                    routes = fetchRoutesFromDatabase(database = database)
                },
                modifier = Modifier.padding(top = 8.dp)
            ) {
                Text(text = "Clear map")
            }
        }
        PopUpRouteInfo(
            routeInfo = routeInfo,
            boxModifier = Modifier
                .fillMaxSize()
                .background(Color.White.copy(alpha = 0.9f))
                .padding(32.dp)
                .align(Alignment.Center),
            showRouteInfo = showRouteInfo,
            onShowRouteChange = { showRouteInfo = it }
        )
    }
}

@Composable
fun PopUpRouteInfo(routeInfo: RouteInfo?, boxModifier: Modifier, showRouteInfo: Boolean, onShowRouteChange: (Boolean)->Unit){
    if (showRouteInfo && routeInfo != null) {
        Box(
            modifier = boxModifier
        ) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp)
                    .align(Alignment.Center),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ) {
                Text(
                    text = "Route Information",
                    modifier = Modifier.padding(bottom = 8.dp)
                )
                Text(
                    text = "ID: ${routeInfo.idTrasy}",
                    modifier = Modifier.padding(bottom = 8.dp)
                )
                Text(
                    text = "Name: ${routeInfo.name}",
                    modifier = Modifier.padding(bottom = 8.dp)
                )
                Text(
                    text = "Time: ${routeInfo.overallTime} s",
                    modifier = Modifier.padding(bottom = 8.dp)
                )
                Text(
                    text = "Distance: ${BigDecimal(routeInfo.distance).setScale(2, RoundingMode.HALF_UP).toDouble()} km",
                    modifier = Modifier.padding(bottom = 16.dp)
                )
                Text(
                    text = "Speed: ${BigDecimal(routeInfo.speed).setScale(2, RoundingMode.HALF_UP).toDouble()} m/s",
                    modifier = Modifier.padding(bottom = 16.dp)
                )
                Button(
                    onClick = {
                        onShowRouteChange(false)
                    }
                ) {
                    Text(text = "Close", color = Color.White)
                }
            }
        }
    }
}

@Composable
fun DropdownMenu(routes: MutableList<Route>, selectedRoute: Route?, onDelete: (Route) -> Unit, onSelect: (Route) -> Unit) {
    var expanded by remember { mutableStateOf(false) }
    Box(modifier = Modifier.fillMaxWidth()) {
        Text(
            text = selectedRoute?.name ?: "Select a route",
            modifier = Modifier
                .fillMaxWidth()
                .clickable {
                    expanded = true
                }
                .background(Color.White)
                .padding(16.dp)
        )
        androidx.compose.material3.DropdownMenu(
            expanded = expanded,
            onDismissRequest = { expanded = false }
        ) {
            routes.forEach { route ->
                DropdownMenuItem(
                    onClick = {
                        onSelect(route)
                        expanded = false
                    },
                    text = {
                        Row(
                            verticalAlignment = Alignment.CenterVertically,
                            horizontalArrangement = Arrangement.SpaceBetween,
                            modifier = Modifier.fillMaxWidth()
                        ) {
                            Text(text = route.name)
                            IconButton(onClick = {
                                onDelete(route)
                                expanded = false
                            }) {
                                Icon(
                                    imageVector = Icons.Filled.Clear,
                                    contentDescription = "Delete Route"
                                )
                            }
                        }
                    }
                )
            }
        }
    }
}