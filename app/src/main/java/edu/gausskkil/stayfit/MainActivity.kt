package edu.gausskkil.stayfit

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.runtime.Composable
import androidx.navigation.compose.rememberNavController
import androidx.room.Room
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import edu.gausskkil.stayfit.ui.theme.StayFitTheme

class MainActivity : ComponentActivity() {
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        val database = Room.databaseBuilder(applicationContext, AppDatabase::class.java, "stayfit.db")
            .allowMainThreadQueries()
            .fallbackToDestructiveMigration()
            .build()

//        database.routesDaoInfo().purge()
//        database.routesDaoDetails().purge()
        Log.i("DATABASE", "Data from database has been deleted.")
        addTestRoute(database = database)
        Log.i("DATABASE", "Number of routes: ${database.routesDaoInfo().getNumberOfRoutes()}")
        database.settingsDao().purge()
        database.settingsDao().insertSetting(Setting(name = "Temperature", 15))
        database.settingsDao().insertSetting(Setting(name = "Humidity", 40))
        database.settingsDao().insertSetting(Setting(name = "Pressure", 1013))
        database.settingsDao().insertSetting(Setting(name = "Default break between sets", 0))

        setContent {
            StayFitTheme {
                StayFit(database = database)
            }
        }
    }
}