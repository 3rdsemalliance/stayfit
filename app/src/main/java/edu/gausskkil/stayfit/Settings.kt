package edu.gausskkil.stayfit

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material3.Button
import androidx.compose.material3.Divider
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Slider
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import kotlinx.coroutines.launch

class SettingsRepository(private val settingsDao: SettingsDao) {
    fun getSettings() = settingsDao.getAllSettings()
    fun getSetting(name: String) = settingsDao.getValueFromName(name)
    suspend fun updateSetting(setting: Setting) = settingsDao.updateSetting(setting)
}

class SettingsViewModel(private val repository: SettingsRepository) : ViewModel() {
    val settings: LiveData<List<Setting>> = liveData {
        emit(repository.getSettings())
    }

    fun getSetting(name: String): LiveData<Int> = liveData {
        emit(repository.getSetting(name))
    }

    fun updateSetting(setting: Setting) = viewModelScope.launch {
        repository.updateSetting(setting)
    }
}

class SettingsViewModelFactory(private val repository: SettingsRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SettingsViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return SettingsViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}

@Composable
fun Settings(viewModel: SettingsViewModel, navController: NavHostController) {
    val settings by viewModel.settings.observeAsState(emptyList())

    LazyColumn(modifier = Modifier.fillMaxSize()) {
        item {
            Text(
                text = "Settings",
                modifier = Modifier.fillMaxWidth().background(MaterialTheme.colorScheme.primary),
                color = MaterialTheme.colorScheme.secondary,
                textAlign = TextAlign.Center,
                fontSize = 30.sp
            )
            HorizontalDivider(modifier = Modifier.background(MaterialTheme.colorScheme.secondary), color = MaterialTheme.colorScheme.secondary)
        }
        items(settings) { setting ->
            SettingItem(setting, navController)
        }
    }
}

@Composable
fun SettingItem(setting: Setting, navController: NavHostController) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .clickable {
                navController.navigate("edit/${setting.name}")
            }
            .padding(16.dp)
    ) {
        Icon(imageVector = Icons.Default.Settings, contentDescription = null)
        Spacer(modifier = Modifier.width(16.dp))
        Text(text = setting.name, fontSize = 20.sp)
//        Spacer(modifier = Modifier.width(5.dp))
//        Text(text = setting.value.toString(), fontSize = 20.sp)
    }
}

@Composable
fun EditSettingScreen(viewModel: SettingsViewModel, settingName: String, navController: NavHostController) {
    val value by viewModel.getSetting(settingName).observeAsState(initial = null)
    var editedValue by remember { mutableIntStateOf(value ?: 0) }
    var textFieldValue by remember { mutableStateOf(editedValue.toString()) }
    var isValidInput by remember { mutableStateOf(true) }

    LaunchedEffect(value) {
        value?.let {
            editedValue = it
            textFieldValue = it.toString()
        }
    }

    Column(modifier = Modifier.fillMaxSize().padding(16.dp)) {
        Text(text = settingName, fontSize = 24.sp, modifier = Modifier.padding(bottom = 8.dp))

        TextField(
            value = textFieldValue,
            onValueChange = {
                textFieldValue = it
                editedValue = it.toIntOrNull() ?: editedValue
                isValidInput = it.toIntOrNull() != null
            },
            label = { Text("New value") },
            isError = !isValidInput,
            modifier = Modifier.padding(vertical = 16.dp)
        )

        if (!isValidInput) {
            Text(text = "Invalid number", color = MaterialTheme.colorScheme.error)
        }

        Text(text = "Value: $editedValue", fontSize = 20.sp)
        Spacer(modifier = Modifier.height(16.dp))
        Row {
            Button(onClick = { navController.popBackStack() }) {
                Text("Back")
            }
            Spacer(modifier = Modifier.width(16.dp))
            Button(onClick = {
                if (isValidInput) {
                    viewModel.updateSetting(Setting(name = settingName, value = editedValue))
                    navController.popBackStack()
                }
            }) {
                Text("Modify")
            }
        }
    }
}

@Composable
fun AppNavHost(navController: NavHostController, viewModel: SettingsViewModel) {
    NavHost(navController, startDestination = "settings") {
        composable("settings") {
            Settings(viewModel, navController)
        }
        composable("edit/{settingName}") { backStackEntry ->
            val settingName = backStackEntry.arguments?.getString("settingName")
            if (settingName != null) {
                EditSettingScreen(viewModel, settingName, navController)
            }
        }
    }
}

@Composable
fun SettingsScreen(database: AppDatabase) {
    val settingsDao = database.settingsDao()
    val repository = SettingsRepository(settingsDao)
    val viewModel: SettingsViewModel = viewModel(factory = SettingsViewModelFactory(repository))
    val navController = rememberNavController()

    AppNavHost(navController, viewModel)
}