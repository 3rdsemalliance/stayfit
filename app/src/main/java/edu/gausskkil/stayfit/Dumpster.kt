package edu.gausskkil.stayfit

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.compose.CameraPositionState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

fun requestLocationPermissionAndFetchLocation(fusedLocationClient: FusedLocationProviderClient, context: Context, cameraPositionState: CameraPositionState, coroutineScope: CoroutineScope) {
    if (ContextCompat.checkSelfPermission(
            context,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) != PackageManager.PERMISSION_GRANTED
    ) {
        ActivityCompat.requestPermissions(
            context as ComponentActivity,
            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
            LOCATION_PERMISSION_REQUEST_CODE
        )
    } else {
        fetchLocation(fusedLocationClient = fusedLocationClient, coroutineScope = coroutineScope, cameraPositionState = cameraPositionState, context = context)
    }
}

@SuppressLint("MissingPermission")
fun fetchLocation(fusedLocationClient: FusedLocationProviderClient, coroutineScope: CoroutineScope, cameraPositionState: CameraPositionState, context: Context){
    fusedLocationClient.lastLocation.addOnSuccessListener { location ->
        if (location != null) {
            coroutineScope.launch {
                cameraPositionState.animate(CameraUpdateFactory.newLatLng(LatLng(location.latitude, location.longitude)))
            }
        } else {
            Toast.makeText(context, "Unable to fetch current location", Toast.LENGTH_SHORT)
                .show()
        }
    }.addOnFailureListener { e ->
        Toast.makeText(context, "Error: ${e.message}", Toast.LENGTH_SHORT).show()
    }
}

//fun moveCameraToCurrentLocation(cameraPositionState: CameraPositionState, coroutineScope: CoroutineScope, fusedLocationClient: FusedLocationProviderClient) {
//    getCurrentLatLng(fusedLocationClient) { latLng ->
//        coroutineScope.launch {
//            cameraPositionState.animate(CameraUpdateFactory.newLatLng(latLng))
//        }
//    }
//}

private const val LOCATION_PERMISSION_REQUEST_CODE = 1001