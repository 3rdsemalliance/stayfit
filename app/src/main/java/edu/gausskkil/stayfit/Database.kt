package edu.gausskkil.stayfit

import android.content.Context
import androidx.room.ColumnInfo
import androidx.room.Dao
import androidx.room.Database
import androidx.room.Delete
import androidx.room.Entity
import androidx.room.Insert
import androidx.room.PrimaryKey
import androidx.room.Query
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.Update

@Database(entities = [RouteDetail::class, RouteInfo::class, Routine::class, Exercise::class, Setting::class], version = 11)
abstract class AppDatabase : RoomDatabase() {
    abstract fun routesDaoInfo(): RoutesDaoInfo
    abstract fun routesDaoDetails(): RoutesDaoDetails
    abstract fun routinesDao(): RoutineDao
    abstract fun exercisesDao(): ExerciseDao

    abstract fun settingsDao(): SettingsDao

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                AppDatabase::class.java, "stayfit.db"
            ).build()
    }
}

@Entity(tableName = "RoutesDetails")
data class RouteDetail(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    @ColumnInfo(name = "ID_trasy") val idTrasy: Int,
    @ColumnInfo(name = "ID_punktu") val idPunktu: Int?,
    @ColumnInfo(name = "latitude") val latitude: Double,
    @ColumnInfo(name = "longitude") val longitude: Double,
    @ColumnInfo(name = "continuation") val continuation: Char?
)

@Entity(tableName = "RoutesInfo")
data class RouteInfo(
    @PrimaryKey @ColumnInfo(name = "ID_trasy") val idTrasy: Int,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "overall_time") val overallTime: Long,
    @ColumnInfo(name = "distance") val distance: Double,
    @ColumnInfo(name = "speed") val speed: Double
)

@Entity(tableName = "Routines")
data class Routine(
    @PrimaryKey @ColumnInfo(name = "RoutineID") val routineID: Int,
    @ColumnInfo(name = "RoutineName") val routineName: String,
    @ColumnInfo(name = "DayOfWeek") val dayOfWeek: Int,
    @ColumnInfo(name = "Time") val time: String
)

@Entity(tableName = "Exercises")
data class Exercise(
    @PrimaryKey @ColumnInfo(name = "ExerciseID") val exerciseID: Int,
    @ColumnInfo(name = "RoutineID") val routineID: Int,
    @ColumnInfo(name = "Name") var name: String,
    @ColumnInfo(name = "Place") val place: Int,
    @ColumnInfo(name = "Type") val type: Char,
    @ColumnInfo(name = "Sets") val sets: Int,
    @ColumnInfo(name = "Reps") val reps: Int
)
@Entity(tableName = "Settings")
data class Setting(
    @PrimaryKey @ColumnInfo(name = "Name") val name: String,
    @ColumnInfo(name = "Value") val value: Int
)

@Dao
interface RoutesDaoInfo {
    @Insert
    fun insertRouteInfo(routeInfo: RouteInfo)

    @Query("SELECT COUNT(*) FROM RoutesInfo")
    fun getNumberOfRoutes(): Int

    @Query("SELECT COALESCE(MAX(ID_trasy),-1) + 1 FROM RoutesInfo")
    fun getNextRouteIndex(): Int

    @Query("DELETE FROM RoutesInfo")
    fun purge()

    @Query("SELECT * FROM RoutesInfo")
    fun fetchAll(): List<RouteInfo>?

    @Query("DELETE FROM RoutesInfo WHERE ID_trasy = (:id)")
    fun deleteRouteInfo(id: Int)

    @Query("SELECT * FROM RoutesInfo WHERE ID_trasy = (:id)")
    fun fetchRouteInfoFromID(id: Int): RouteInfo?
}

@Dao
interface RoutesDaoDetails {
    @Insert
    fun insertRouteDetail(routeDetail: RouteDetail)

    @Query("DELETE FROM RoutesDetails")
    fun purge()

    @Query("SELECT * FROM RoutesDetails WHERE ID_trasy = (:idTrasy)")
    fun getRoutePoints(idTrasy: Int): MutableList<RouteDetail>?

    @Query("DELETE FROM RoutesDetails WHERE ID_trasy = (:id)")
    fun deleteRouteDetails(id: Int)
}

@Dao
interface RoutineDao{
    @Insert
    fun insertRoutine(routine: Routine)

    @Update
    fun updateRoutine(routine: Routine)

    @Delete
    fun deleteRoutine(routine: Routine)

    @Query("DELETE FROM Routines")
    fun purge()

    @Query("SELECT * FROM Routines")
    fun getAllRoutines(): MutableList<Routine>?

    @Query("SELECT * from Routines where RoutineID = (:id)")
    fun getRoutine(id: Int): Routine

    @Query("SELECT routineId from Routines order by routineId desc")
    fun getIDs(): MutableList<Int>?

    @Query("SELECT COALESCE(MAX(routineID),-1) + 1 FROM Routines")
    fun getNextID(): Int
}

@Dao
interface ExerciseDao{
    @Insert
    fun insertExercise(exercise: Exercise)

    @Update
    fun updateExercise(exercise: Exercise)

    @Delete
    fun deleteExercise(exercise: Exercise)

    @Query("DELETE FROM Exercises")
    fun purge()

    @Query("SELECT * FROM Exercises WHERE RoutineID = (:id)")
    fun getExerciseFromRoutine(id: Int): MutableList<Exercise>?

    @Query("SELECT * FROM Exercises WHERE ExerciseID = (:id)")
    fun getExerciseById(id: Int): Exercise?

    @Query("SELECT COALESCE(MAX(exerciseID),-1) + 1 FROM Exercises")
    fun getNextID(): Int
}

@Dao
interface SettingsDao{
    @Insert
    fun insertSetting(setting: Setting)

    @Update
    fun updateSetting(setting: Setting)

    @Query("DELETE FROM Settings")
    fun purge()

    @Query("SELECT Value FROM Settings WHERE Name like (:sName)")
    fun getValueFromName(sName: String): Int

    @Query("SELECT * FROM Settings")
    fun getAllSettings(): List<Setting>
}