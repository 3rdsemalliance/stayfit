package edu.gausskkil.stayfit

import android.app.PendingIntent.getActivity
import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.consumeWindowInsets
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Build
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material3.Button
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Snackbar
import androidx.compose.material3.SnackbarData
import androidx.compose.material3.SnackbarDuration
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.saveable.rememberSaveableStateHolder
import androidx.compose.runtime.setValue
import androidx.compose.runtime.toMutableStateMap
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


data class SectionData(val index: Int, val headerText: String, val items: List<String>)
@Composable
fun SectionItem(text: String) {
    Text(
        text = text,
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 8.dp, horizontal = 16.dp)
    )
}
@Composable
fun SectionHeader(text: String, isExpanded: Boolean, onHeaderClicked: () -> Unit, index: Int, navController: NavController) {
    Row(modifier = Modifier
        .clickable { onHeaderClicked() }
        .background(MaterialTheme.colorScheme.primary)
        .padding(vertical = 16.dp, horizontal = 16.dp)) {
        Text(
            text = text,
//            style = MaterialTheme.typography.h6,
            modifier = Modifier.weight(1.0f),
            fontSize = 20.sp
        )
        Icon(Icons.Filled.Build, contentDescription = null, modifier = Modifier.clickable{
            navController.navigate(RoutineScreens.RoutineEdit.path + "/$index")
        })
        Spacer(modifier = Modifier.width(10.dp))
        Icon(Icons.Filled.Delete, contentDescription = null, modifier = Modifier.clickable{
            navController.navigate(RoutineScreens.RoutineDelete.path + "/$index")
        })
        Spacer(modifier = Modifier.width(10.dp))
        if (isExpanded) {
            Icon(Icons.Filled.Close, contentDescription = null)
        } else {
            Icon(Icons.Filled.Menu, contentDescription = null)
        }
    }
}
@Composable
fun ExpandableList(sections: List<SectionData>, navController: NavController, supportingNavController: NavController) {
    val isExpandedMap = remember {
        List(sections.size) { index: Int -> index to false }
            .toMutableStateMap()
    }

    LazyColumn(){
        item{
            HorizontalDivider(modifier = Modifier.background(MaterialTheme.colorScheme.secondary), color = MaterialTheme.colorScheme.secondary)
            Text("Your routines", modifier = Modifier
                .background(MaterialTheme.colorScheme.primary)
                .fillMaxWidth(), fontSize = 30.sp, textAlign = TextAlign.Center)
            HorizontalDivider(modifier = Modifier.background(MaterialTheme.colorScheme.secondary), color = MaterialTheme.colorScheme.secondary)
        }
        sections.onEachIndexed { index, sectionData ->
            Section(
                sectionData = sectionData,
                isExpanded = isExpandedMap[index] ?: true,
                onHeaderClick = {
                    isExpandedMap[index] = !(isExpandedMap[index] ?: true)
                },
                navController = navController,
                supportingNavController = supportingNavController
            )
        }
    }
}
fun LazyListScope.Section(
    sectionData: SectionData,
    isExpanded: Boolean,
    onHeaderClick: () -> Unit,
    navController: NavController,
    supportingNavController: NavController
) {

    item {
        SectionHeader(
            text = sectionData.headerText,
            isExpanded = isExpanded,
            onHeaderClicked = onHeaderClick,
            navController = supportingNavController,
            index = sectionData.index
        )
    }

    if(isExpanded) {
        items(sectionData.items) {
            SectionItem(text = it)
        }
        item{
            Column(modifier = Modifier.fillMaxWidth(), horizontalAlignment = Alignment.CenterHorizontally){
                Button(modifier = Modifier.padding(bottom = 10.dp), onClick = {
                    navController.navigate(Screens.Exercise.route + "/${sectionData.index}")
                }){
                    Text("Let's do this!")
                }
            }
        }
    }
}

sealed class RoutineScreens(val path: String){
    data object RoutineList: RoutineScreens("RoutineList")
    data object RoutineAdd: RoutineScreens("RoutineAdd")
    data object RoutineEdit: RoutineScreens("RoutineEdit")
    data object RoutinePropertiesEdit: RoutineScreens("RoutinePropertiesEdit")
    data object RoutineDelete: RoutineScreens("RoutineDelete")
    data object ExerciseAdd: RoutineScreens("ExerciseAdd")
    data object ExerciseEdit: RoutineScreens("ExerciseEdit")
    data object ExerciseDelete: RoutineScreens("ExerciseDelete")

}
@Composable
fun RoutineNavigation(navController: NavController, database: AppDatabase){
    val internalNavController = rememberNavController()
    NavHost(navController = internalNavController, startDestination = RoutineScreens.RoutineList.path){
        composable(route = RoutineScreens.RoutineList.path) {
            RoutineList(navController = navController, database = database, internalNavController = internalNavController)
        }
        composable(route = RoutineScreens.RoutineAdd.path){
            addRoutineScreen(database)
        }
        composable(
            route = RoutineScreens.RoutineEdit.path + "/{routineID}",
            arguments = listOf(
                navArgument("routineID") {
                    type = NavType.IntType
                }
            )
        ){ entry ->
            entry.arguments?.getInt("routineID")
                ?.let { editRoutineScreen(index = it, database = database, navController = internalNavController) }
        }
        composable(
            route = RoutineScreens.RoutinePropertiesEdit.path + "/{routineID}",
            arguments = listOf(
                navArgument("routineID") {
                    type = NavType.IntType
                }
            )
        ){ entry ->
            entry.arguments?.getInt("routineID")
                ?.let { editRoutinePropertiesScreen(routineIndex = it, database = database) }
        }
        composable(
            route = RoutineScreens.RoutineDelete.path + "/{routineID}",
            arguments = listOf(
                navArgument("routineID") {
                    type = NavType.IntType
                }
            )
        ){ entry ->
            entry.arguments?.getInt("routineID")
                ?.let { deleteRoutineScreen(index = it, database = database) }
        }
        composable(
            route = RoutineScreens.ExerciseAdd.path + "/{routineID}",
            arguments = listOf(
                navArgument("routineID") {
                    type = NavType.IntType
                }
            )
        ){ entry ->
            entry.arguments?.getInt("routineID")
                ?.let { addExerciseScreen(routineIndex = it, database = database) }
        }
        composable(
            route = RoutineScreens.ExerciseEdit.path + "/{exerciseID}",
            arguments = listOf(
                navArgument("exerciseID") {
                    type = NavType.IntType
                }
            )
        ){ entry ->
            entry.arguments?.getInt("exerciseID")
                ?.let { editExerciseScreen(exerciseIndex = it, database = database, navController = internalNavController) }
        }
        composable(
            route = RoutineScreens.ExerciseDelete.path + "/{exerciseID}",
            arguments = listOf(
                navArgument("exerciseID") {
                    type = NavType.IntType
                }
            )
        ){ entry ->
            entry.arguments?.getInt("exerciseID")
                ?.let { deleteExerciseScreen(exerciseIndex = it, database = database) }
        }
    }
}
@Composable
fun RoutineList(navController: NavController, database: AppDatabase, internalNavController: NavController){
    val routineList = database.routinesDao().getAllRoutines()
    val dataList = mutableListOf<SectionData>()
    if(routineList != null){
        for (i in 0..<routineList.size){
            val exerciseList = database.exercisesDao().getExerciseFromRoutine(i)
            val exerciseNames = mutableListOf<String>()
            exerciseNames.add("${routineList[i].dayOfWeek}, ${routineList[i].time}")
            if(exerciseList != null){
                for(j in 0..<exerciseList.size){
                    var textToAdd = "  - ${exerciseList[j].name}, ${exerciseList[j].sets}x${exerciseList[j].reps}"
                    if (exerciseList[j].type == 'W'){
                        textToAdd += 's'
                    }
                    exerciseNames.add(textToAdd)
                }
            }
            dataList.add(SectionData(routineList[i].routineID, routineList[i].routineName, exerciseNames))
        }
    }
    Scaffold(floatingActionButton = {
            FloatingActionButton(
                onClick = {
                    internalNavController.navigate(RoutineScreens.RoutineAdd.path)
                },
                modifier = Modifier.padding(15.dp)
            ) {
                Icon(Icons.Filled.Add, "Floating action button")
            }
    }){
        Column(modifier = Modifier.consumeWindowInsets(it)) {
            ExpandableList(sections = dataList, navController = navController, supportingNavController = internalNavController)
        }
    }
}

@Composable
fun addRoutineScreen(database: AppDatabase){
    var editedName by remember { mutableStateOf( "") }
    var nameFieldValue by remember { mutableStateOf(editedName) }

    var editedDay by remember {mutableStateOf("1")}
    var dayFieldValue by remember { mutableStateOf(editedDay) }

    var editedTime by remember { mutableStateOf("00:00")}
    var timeFieldValue by remember {mutableStateOf(editedTime)}
    val snackbarHostState = remember { SnackbarHostState() }

    val scope = rememberCoroutineScope()
    Scaffold(snackbarHost = {
        SnackbarHost(hostState = snackbarHostState)
    }){ paddingValues ->
        Column(modifier = Modifier.consumeWindowInsets(paddingValues)){
            HorizontalDivider(color = MaterialTheme.colorScheme.secondary)
            Text("Add new routine", modifier = Modifier.fillMaxWidth(), fontSize = 30.sp, textAlign = TextAlign.Center)

            Column{
                TextField(
                    value = nameFieldValue,
                    onValueChange = {
                        nameFieldValue = it
                    },
                    label = { Text("Routine name") },
                    modifier = Modifier.padding(vertical = 16.dp)
                )
            }
            Row{
                TextField(
                    value = dayFieldValue,
                    onValueChange = {
                        dayFieldValue = it
                        editedDay = it
                    },
                    label = { Text("Day of week(1-7)") },
                    modifier = Modifier.padding(vertical = 16.dp)
                )
            }
            Row{
                TextField(
                    value = timeFieldValue,
                    onValueChange = {
                        timeFieldValue = it
                    },
                    label = { Text("Time") },
                    modifier = Modifier.padding(vertical = 16.dp)
                )
            }
            Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.End){
                Button(modifier = Modifier.fillMaxWidth(), onClick = {
                    if(editedName == ""){
                        database.routinesDao().insertRoutine(Routine(database.routinesDao().getNextID(), nameFieldValue, dayFieldValue.toInt(), timeFieldValue))
                        scope.launch{
                            snackbarHostState.showSnackbar(
                                message = "Routine added.",
                                actionLabel = "")
                        }

                    } else {
                        scope.launch{
                            snackbarHostState.showSnackbar(
                                message = "Enter a name first!",
                                actionLabel = "")
                        }
                    }
                }){
                    Text("Save")
                }
            }
            HorizontalDivider(color = MaterialTheme.colorScheme.secondary)
        }
    }
}
@Composable
fun editRoutineScreen(index: Int, database: AppDatabase, navController: NavController){
    val routine = database.routinesDao().getRoutine(index)
    val exerciseList = database.exercisesDao().getExerciseFromRoutine(index)
    Scaffold(){
        Column(modifier = Modifier.consumeWindowInsets(it)){
            HorizontalDivider(color = MaterialTheme.colorScheme.secondary)
            Text(text = routine.routineName, fontSize = 30.sp, modifier = Modifier.fillMaxWidth(), textAlign = TextAlign.Center)
            Button(modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 20.dp), onClick = {
                navController.navigate(RoutineScreens.RoutinePropertiesEdit.path + "/$index")
            }){
                Text("Change this routine's properties")
            }
            Text("Tap an exercise to edit its properties.")
            Text("Current exercise list for this routine:")
            if(exerciseList != null){
                for (i in 0..<exerciseList.size){
                    Row(modifier = Modifier
                        .fillMaxWidth()
                        .clickable {
                            navController.navigate(RoutineScreens.ExerciseEdit.path + "/${exerciseList[i].exerciseID}")
                        }){
                        Text("${exerciseList[i].name}: ${exerciseList[i].sets}x${exerciseList[i].reps}")
                    }
                }
            } else {
                Text("There are no exercises in this routine.")
            }
            Button(onClick = {
                navController.navigate(RoutineScreens.ExerciseAdd.path + "/${index}")
            }){
                Text("Add an exercise")
            }
        }
    }
}
@Composable
fun editRoutinePropertiesScreen(routineIndex: Int, database: AppDatabase){
    val routineInfo = database.routinesDao().getRoutine(routineIndex)
    var editedName by remember { mutableStateOf( routineInfo.routineName) }
    var nameFieldValue by remember { mutableStateOf(editedName) }

    var editedDay by remember {mutableStateOf(routineInfo.dayOfWeek.toString())}
    var dayFieldValue by remember { mutableStateOf(editedDay) }
    var isValidInput by remember { mutableStateOf(true) }

    var editedTime by remember { mutableStateOf(routineInfo.time)}
    var timeFieldValue by remember {mutableStateOf(editedTime)}
    val snackbarHostState = remember { SnackbarHostState() }

    val scope = rememberCoroutineScope()
    Scaffold(snackbarHost = {
        SnackbarHost(hostState = snackbarHostState)
    }){ paddingValues ->
        Column(modifier = Modifier.consumeWindowInsets(paddingValues)){
            HorizontalDivider(color = MaterialTheme.colorScheme.secondary)
            Text("Edit routine info", modifier = Modifier.fillMaxWidth(), fontSize = 30.sp, textAlign = TextAlign.Center)

            Column{
                TextField(
                    value = nameFieldValue,
                    onValueChange = {
                        nameFieldValue = it
                        editedName = it
                    },
                    label = { Text("Routine name") },
                    modifier = Modifier.padding(vertical = 16.dp)
                )
            }
            Row{
                TextField(
                    value = dayFieldValue,
                    onValueChange = {
                        dayFieldValue = it
                        editedDay = it
                    },
                    label = { Text("Day of week(1-7)") },
                    modifier = Modifier.padding(vertical = 16.dp)
                )
            }
            Row{
                TextField(
                    value = timeFieldValue,
                    onValueChange = {
                        timeFieldValue = it
                        editedTime = it
                    },
                    label = { Text("Time") },
                    modifier = Modifier.padding(vertical = 16.dp)
                )
            }
            Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.End){
                Button(modifier = Modifier.fillMaxWidth(), onClick = {
                    if(editedName != ""){
                        val newData = Routine(routineInfo.routineID, nameFieldValue, dayFieldValue.toInt(), timeFieldValue)
                        database.routinesDao().updateRoutine(newData)
                        scope.launch{
                            snackbarHostState.showSnackbar(
                                message = "Routine modified.",
                                actionLabel = "")
                        }

                    } else {
                        scope.launch{
                            snackbarHostState.showSnackbar(
                                message = "Enter a name first!",
                                actionLabel = "")
                        }
                    }
                }){
                    Text("Save changes")
                }
            }
            HorizontalDivider(color = MaterialTheme.colorScheme.secondary)
        }
    }
}

@Composable
fun deleteRoutineScreen(index: Int, database: AppDatabase){
    val snackbarHostState = remember { SnackbarHostState() }

    val scope = rememberCoroutineScope()
    val routine = database.routinesDao().getRoutine(index)
    Scaffold(snackbarHost = {
        SnackbarHost(hostState = snackbarHostState)
    }){ paddingValues ->
        Column(modifier = Modifier.consumeWindowInsets(paddingValues), verticalArrangement = Arrangement.Center, horizontalAlignment = Alignment.CenterHorizontally){
            HorizontalDivider(color = MaterialTheme.colorScheme.secondary)
            Text("Are you sure? This will result in deleting ${routine.routineName}!", modifier = Modifier.fillMaxWidth(), fontSize = 30.sp, textAlign = TextAlign.Center)
            Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.End){
                Button(modifier = Modifier.fillMaxWidth(), onClick = {
                    val exercises = database.exercisesDao().getExerciseFromRoutine(index)
                    if(exercises != null){
                        for(i in 0..<exercises.size){
                            database.exercisesDao().deleteExercise(exercises[i])
                        }
                    }
                    database.routinesDao().deleteRoutine(routine)
                    scope.launch{
                        snackbarHostState.showSnackbar(
                            message = "${routine.routineName} deleted.",
                            actionLabel = "")
                    }
                }){
                    Text("Yes, delete it")
                }
            }
            HorizontalDivider(color = MaterialTheme.colorScheme.secondary)
        }
    }
}
@Composable
fun addExerciseScreen(routineIndex: Int, database: AppDatabase){
    var editedName by remember { mutableStateOf( "") }
    var nameFieldValue by remember { mutableStateOf(editedName) }

    var editedType by remember {mutableStateOf("S")}
    var typeFieldValue by remember {mutableStateOf(editedType)}

    var editedSets by remember {mutableStateOf("1")}
    var setsFieldValue by remember { mutableStateOf(editedSets) }

    var editedReps by remember {mutableStateOf("1")}
    var repsFieldValue by remember { mutableStateOf(editedReps) }

    val snackbarHostState = remember { SnackbarHostState() }
    val scope = rememberCoroutineScope()

    Scaffold(snackbarHost = {
        SnackbarHost(hostState = snackbarHostState)
    }){ paddingValues ->
        Column(modifier = Modifier.consumeWindowInsets(paddingValues)){
            HorizontalDivider(color = MaterialTheme.colorScheme.secondary)
            Text("Add new exercise", modifier = Modifier.fillMaxWidth(), fontSize = 30.sp, textAlign = TextAlign.Center)

            Row{
                TextField(
                    value = nameFieldValue,
                    onValueChange = {
                        nameFieldValue = it
                        editedName = it
                    },
                    label = { Text("Exercise name") },
                    isError = false,
                    modifier = Modifier.padding(vertical = 16.dp)
                )
            }
            Row{
                TextField(
                    value = typeFieldValue,
                    onValueChange = {
                        typeFieldValue = it
                        editedType = it
                    },
                    label = { Text("Exercise type (S/W)") },
                    isError = false,
                    modifier = Modifier.padding(vertical = 16.dp)
                )
            }
            Row{
                TextField(
                    value = setsFieldValue.toString(),
                    onValueChange = {
                        setsFieldValue = it
                        editedSets = it
                    },
                    label = { Text("Sets") },
                    modifier = Modifier.padding(vertical = 16.dp)
                )
            }
            Row{
                TextField(
                    value = repsFieldValue.toString(),
                    onValueChange = {
                        repsFieldValue = it
                        editedReps = it
                    },
                    label = { Text("Reps per set") },
                    modifier = Modifier.padding(vertical = 16.dp)
                )
            }
            Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.End){
                Button(modifier = Modifier.fillMaxWidth(), onClick = {
                    if(editedName == ""){
                        scope.launch{
                            snackbarHostState.showSnackbar(message = "Set a name first!")
                        }
                    } else if (editedType == "") {
                        scope.launch{
                            snackbarHostState.showSnackbar(message = "The exercise must have a type!")
                        }
                    } else {
                        database.exercisesDao().insertExercise(Exercise(database.exercisesDao().getNextID(), routineIndex, editedName, 0, editedType[0], editedSets.toInt(), editedReps.toInt()))
                        scope.launch{
                            snackbarHostState.showSnackbar(
                                message = "Exercise added.",
                                actionLabel = "")
                        }
                    }
                }){
                    Text("Save")
                }
            }
            HorizontalDivider(color = MaterialTheme.colorScheme.secondary)
        }
    }
}

@Composable
fun editExerciseScreen(exerciseIndex: Int, database: AppDatabase, navController: NavController){
    var exerciseInfo = database.exercisesDao().getExerciseById(exerciseIndex)
    if(exerciseInfo != null){
        var editedName by remember { mutableStateOf( exerciseInfo.name) }
        var nameFieldValue by remember { mutableStateOf(editedName) }

        var editedType by remember {mutableStateOf(exerciseInfo.type.toString())}
        var typeFieldValue by remember {mutableStateOf(editedType)}

        var editedSets by remember {mutableStateOf(exerciseInfo.sets.toString())}
        var setsFieldValue by remember { mutableStateOf(editedSets) }

        var editedReps by remember {mutableStateOf(exerciseInfo.reps.toString())}
        var repsFieldValue by remember { mutableStateOf(editedReps) }

        val snackbarHostState = remember { SnackbarHostState() }
        val scope = rememberCoroutineScope()

        Scaffold(snackbarHost = {
            SnackbarHost(hostState = snackbarHostState)
        }){ paddingValues ->
            Column(modifier = Modifier.consumeWindowInsets(paddingValues)){
                HorizontalDivider(color = MaterialTheme.colorScheme.secondary)
                Text("Set exercise info", modifier = Modifier.fillMaxWidth(), fontSize = 30.sp, textAlign = TextAlign.Center)

                Row{
                    TextField(
                        value = nameFieldValue,
                        onValueChange = {
                            nameFieldValue = it
                            editedName = it
                        },
                        label = { Text("Exercise name") },
                        isError = false,
                        modifier = Modifier.padding(vertical = 16.dp)
                    )
                }
                Row{
                    TextField(
                        value = typeFieldValue,
                        onValueChange = {
                            typeFieldValue = it
                            editedType = it
                        },
                        label = { Text("Exercise type (S/W)") },
                        isError = false,
                        modifier = Modifier.padding(vertical = 16.dp)
                    )
                }
                Row{
                    TextField(
                        value = setsFieldValue,
                        onValueChange = {
                            setsFieldValue = it
                            editedSets = it
                        },
                        label = { Text("Sets") },
                        modifier = Modifier.padding(vertical = 16.dp)
                    )
                }
                Row{
                    TextField(
                        value = repsFieldValue,
                        onValueChange = {
                            repsFieldValue = it
                            editedReps = it
                        },
                        label = { Text("Reps per set") },
                        modifier = Modifier.padding(vertical = 16.dp)
                    )
                }
                Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.End){
                    Button(onClick = {
                        navController.navigate(RoutineScreens.ExerciseDelete.path + "/$exerciseIndex")
                    }){
                        Text("Delete exercise")
                    }
                    Button(onClick = {
                        if(editedName == ""){
                            scope.launch{
                                snackbarHostState.showSnackbar(message = "The exercise must have a name!")
                            }
                        } else if (editedType == "") {
                            scope.launch{
                                snackbarHostState.showSnackbar(message = "The exercise must have a type!")
                            }
                        } else {
                            val newData = Exercise(
                                exerciseInfo.exerciseID,
                                exerciseInfo.routineID,
                                editedName,
                                exerciseInfo.place,
                                editedType[0],
                                editedSets.toInt(),
                                editedReps.toInt()
                            )

                            database.exercisesDao().updateExercise(newData)
                            scope.launch{
                                snackbarHostState.showSnackbar(
                                    message = "Exercise updated.",
                                    actionLabel = "")
                            }
                        }
                    }){
                        Text("Save changes")
                    }
                }
                HorizontalDivider(color = MaterialTheme.colorScheme.secondary)
            }
        }
    } else {
        Column(modifier = Modifier.fillMaxSize(), horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.Center){
            Text("This exercise has been deleted.")
        }
    }
}
@Composable
fun deleteExerciseScreen(exerciseIndex: Int, database: AppDatabase){
    val snackbarHostState = remember { SnackbarHostState() }

    val scope = rememberCoroutineScope()
    val exercise = database.exercisesDao().getExerciseById(exerciseIndex)
    if(exercise != null){
        Scaffold(snackbarHost = {
            SnackbarHost(hostState = snackbarHostState)
        }){ paddingValues ->
            Column(modifier = Modifier.consumeWindowInsets(paddingValues), verticalArrangement = Arrangement.Center, horizontalAlignment = Alignment.CenterHorizontally){
                HorizontalDivider(color = MaterialTheme.colorScheme.secondary)
                Text("Are you sure? This will result in deleting ${exercise.name}, ${exercise.sets}x${exercise.reps}!", modifier = Modifier.fillMaxWidth(), fontSize = 30.sp, textAlign = TextAlign.Center)
                Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.End){
                    Button(modifier = Modifier.fillMaxWidth(), onClick = {
                        database.exercisesDao().deleteExercise(exercise)
                        scope.launch{
                            snackbarHostState.showSnackbar(
                                message = "${exercise.name} deleted.",
                                actionLabel = "")
                        }
                    }){
                        Text("Yes, delete it")
                    }
                }
                HorizontalDivider(color = MaterialTheme.colorScheme.secondary)
            }
        }
    } else {
        Column(verticalArrangement = Arrangement.Center, horizontalAlignment = Alignment.CenterHorizontally){
            Text("You're in luck! Somehow, this exercise has already been deleted!")
        }
    }

}
@Composable
fun TrainingScreen(id: Int, database: AppDatabase){
    val routineInfo = database.routinesDao().getRoutine(id)
    val exerciseList = database.exercisesDao().getExerciseFromRoutine(id)
    val breakTime = database.settingsDao().getValueFromName("Default break between sets")
//    val breakTime = 1
    var remainingTime by remember {
        mutableIntStateOf(0)
    }
    var index by remember {
        mutableIntStateOf(0)
    }
    var currentSet by remember {
        mutableIntStateOf(1)
    }
    var exerciseTime by remember {
        mutableIntStateOf(0)
    }
    var exerciseInitialised by remember {
        mutableStateOf(false)
    }

    var trainingTime by remember {
        mutableIntStateOf(0)
    }
    var stopTheCount by remember {
        mutableStateOf(false)
    }
    LaunchedEffect(trainingTime) {
        delay(1000)
        if(!stopTheCount){
            ++trainingTime
        }
    }
    if(exerciseList != null){
        Column(modifier = Modifier.fillMaxWidth()){
            Text(modifier = Modifier
                .fillMaxWidth()
                .background(MaterialTheme.colorScheme.primary),
                text = routineInfo.routineName, textAlign = TextAlign.Center, fontSize = 30.sp
            )
            if(!stopTheCount){
                Spacer(modifier = Modifier.height(50.dp))
                Text("-- Duration --", fontSize = 30.sp, textAlign = TextAlign.Center, modifier = Modifier.fillMaxWidth())
                Text(timeToText(trainingTime.toLong()), textAlign = TextAlign.Center, modifier = Modifier.fillMaxWidth(), fontSize = 30.sp)
            }
        }
        Column(horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.Center, modifier = Modifier.fillMaxSize()){

            if(index < exerciseList.size && remainingTime == 0){
                Text(text = "Current exercise:", fontSize = 26.sp)
                if(exerciseList[index].type == 'S'){
                    Text(text = "${exerciseList[index].name} - ${exerciseList[index].reps} reps.", fontSize = 20.sp)
                } else{
                    Text(text = "${exerciseList[index].name} - ${exerciseList[index].reps} seconds.", fontSize = 20.sp)
                }
                Text(text = "Set $currentSet of ${exerciseList[index].sets}")
                if(exerciseList[index].type == 'S'){
                    Button(onClick = {
                        remainingTime = breakTime
                        ++currentSet
                        if(currentSet > exerciseList[index].sets){
                            ++index
                            currentSet = 1
                        }
                    }){
                        Text("Onto the next one!")
                    }
                } else {
                    if(!exerciseInitialised){
                        exerciseTime = exerciseList[index].reps
                        exerciseInitialised = true
                    }
                    LaunchedEffect(exerciseTime) {
                        delay(1000)
                        --exerciseTime
                    }
                    if(exerciseTime <= 0){
                        exerciseTime = 0
                        ++currentSet
                        exerciseInitialised = false
                        if(currentSet > exerciseList[index].sets){
                            ++index
                            currentSet = 1
                        }
                        remainingTime = breakTime
                    }
                    Text("Hold on tight! Remaining time:", fontSize = 26.sp)
                    Text("$exerciseTime", fontSize = 48.sp)

                }
            } else if (remainingTime > 0){
                LaunchedEffect(remainingTime) {
                    delay(1000)
                    --remainingTime
                }
                Text("Break time! Time left: ", fontSize = 24.sp)
                Text("$remainingTime", fontSize = 48.sp, color = MaterialTheme.colorScheme.tertiary)
            } else {
                stopTheCount = true
                Text("You made it!", fontSize = 44.sp, color = MaterialTheme.colorScheme.primary)
                Text("The training's over.", fontSize = 44.sp, color = MaterialTheme.colorScheme.primary)
                Text("Duration: ${timeToText(trainingTime.toLong())}")
            }
        }
    }
}