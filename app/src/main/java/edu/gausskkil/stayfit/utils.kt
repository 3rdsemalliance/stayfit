package edu.gausskkil.stayfit

import android.annotation.SuppressLint
import android.content.Intent
import android.location.Location
import android.net.Uri
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import android.util.Log
import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.MultiplePermissionsState
import com.google.accompanist.permissions.rememberMultiplePermissionsState
import com.google.accompanist.permissions.shouldShowRationale
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.maps.model.LatLng
import kotlin.math.abs

@SuppressLint("MissingPermission")
fun getCurrentLatLng(
    fusedLocationClient: FusedLocationProviderClient,
    onLocationReceived: (LatLng) -> Unit
) {
    val retryInterval: Long = 2000L
    fusedLocationClient.lastLocation.addOnSuccessListener { location ->
        if (location != null) {
            val latLng = LatLng(location.latitude, location.longitude)
            onLocationReceived(latLng)
        } else {
            Handler(Looper.getMainLooper()).postDelayed({
                getCurrentLatLng(fusedLocationClient, onLocationReceived)
            }, retryInterval)
        }
    }.addOnFailureListener { exception ->
        exception.printStackTrace()
        Handler(Looper.getMainLooper()).postDelayed({
            getCurrentLatLng(fusedLocationClient, onLocationReceived)
        }, retryInterval)
    }
}

fun createPaths(locations: MutableList<Location>): MutableList<MutableList<Location>>{
    var paths = mutableListOf<MutableList<Location>>()
    var path = mutableListOf<Location>()
    for (i in 0..<locations.size){
        path.add(locations[i])
        if(locations[i].provider?.last() == 'P' || i == locations.size - 1){
            paths.add(path)
            path = mutableListOf()
        }
    }
    return paths
}

fun measurePathsDistance(paths: MutableList<MutableList<Location>>): Double{
    var sum: Double = 0.0
    for (path in paths){
        for (j in 0..< path.size-1){
            sum += distanceBetweenLocations(path[j], path[j+1])
        }
    }
    return sum
}

fun distanceBetweenLocations(locationA: Location, locationB: Location): Double{
    // return distance value in kilometers
    return locationA.distanceTo(locationB) / 1000.0
}

fun createLocation(lat: Double, lng: Double, name: String): Location{
    val location: Location = Location(name)
    location.latitude = lat
    location.longitude = lng
    return location
}

@SuppressLint("DefaultLocale")
fun timeToText(time: Long): String{
    return String.format("%02d:%02d:%02d", time / 3600, (time % 3600) / 60, time % 60)
}

data class Route(val id: Int, val name: String, val points: MutableList<MutableList<LatLng>>)

fun fetchRoutesFromDatabase(database: AppDatabase): MutableList<Route> {
    val databaseRoutes = database.routesDaoInfo().fetchAll()
    val routes = mutableListOf<Route>()
    if (databaseRoutes != null) {
        for (databaseRoute in databaseRoutes) {
            val routeDetails = database.routesDaoDetails().getRoutePoints(databaseRoute.idTrasy)
            val routePoints = mutableListOf<MutableList<LatLng>>()
            var routePoint = mutableListOf<LatLng>()
            if (routeDetails != null) {
                for (i in routeDetails.indices) {
                    routePoint.add(LatLng(routeDetails[i].latitude, routeDetails[i].longitude))
                    if (routeDetails[i].continuation == 'P' || i == routeDetails.size - 1) {
                        routePoints.add(routePoint)
                        routePoint = mutableListOf()
                    }
                }
            }
            if (routePoints.isNotEmpty()) {
                routes.add(Route(id = databaseRoute.idTrasy, name = databaseRoute.name, points = routePoints))
            }
        }
    }
    return routes
}

fun addTestRoute(database: AppDatabase){
    val nextIndex = database.routesDaoInfo().getNextRouteIndex()
    database.routesDaoInfo().insertRouteInfo(
        RouteInfo(
            idTrasy = nextIndex,
            name = "Test route",
            distance = 10.0,
            overallTime = 100,
            speed = 10.0 / 100 * 1000
        )
    )
    database.routesDaoDetails().insertRouteDetail(
        RouteDetail(
            idTrasy = nextIndex,
            idPunktu = 0,
            continuation = 'C',
            latitude = 52.3960079,
            longitude = 16.9611231
        )
    )
    database.routesDaoDetails().insertRouteDetail(
        RouteDetail(
            idTrasy = nextIndex,
            idPunktu = 1,
            continuation = 'P',
            latitude = 52.4112105,
            longitude = 16.9677615
        )
    )
    database.routesDaoDetails().insertRouteDetail(
        RouteDetail(
            idTrasy = nextIndex,
            idPunktu = 2,
            continuation = 'C',
            latitude = 52.411015,
            longitude = 16.938274
        )
    )
    database.routesDaoDetails().insertRouteDetail(
        RouteDetail(
            idTrasy = nextIndex,
            idPunktu = 3,
            continuation = 'C',
            latitude = 52.399050,
            longitude = 16.935356
        )
    )
}

val REC_COUNTER_DEFAULT_MAX: Double = 6.0
val REC_COUNTER_BIG_STEP: Double = 2.0
val REC_COUNTER_SMALL_STEP: Double = 1.0

fun calculateRecommendation(pressure: Float?, temperature: Float?, humidity: Float?, database: AppDatabase): String{
    val perfectPressure = database.settingsDao().getValueFromName("Pressure"); val pressureMidOffset = 5; val pressureHighOffset = 13; val pressureCriticalOffset = 20
    val perfectTemperature = database.settingsDao().getValueFromName("Temperature"); val temperatureMidOffset = 5; val temperatureHighOffset = 10; val temperatureCriticalOffset = 15
    val perfectHumidity = database.settingsDao().getValueFromName("Humidity"); val humidityMidOffset = 10; val humidityHighOffset = 20; val humidityCriticalOffset = 30

    var counter = 0.0; var counterMax: Double = REC_COUNTER_DEFAULT_MAX

    if(pressure != null){
        if(abs(perfectPressure - pressure) < pressureMidOffset){
            counter += REC_COUNTER_BIG_STEP
        }
        else if(abs(perfectPressure - pressure) < pressureHighOffset){
            counter += REC_COUNTER_SMALL_STEP
        }
    } else {
        counterMax -= REC_COUNTER_BIG_STEP
    }

    if(temperature != null){
        if(abs(perfectTemperature - temperature) < temperatureMidOffset){
            counter += REC_COUNTER_BIG_STEP
        }
        else if(abs(perfectTemperature - temperature) < temperatureHighOffset){
            counter += REC_COUNTER_SMALL_STEP
        }
    } else {
        counterMax -= REC_COUNTER_BIG_STEP
    }

    if(humidity != null){
        if(abs(perfectHumidity - humidity) < humidityMidOffset){
            counter += REC_COUNTER_BIG_STEP
        }
        else if(abs(perfectHumidity - humidity) < humidityHighOffset){
            counter += REC_COUNTER_SMALL_STEP
        }
    } else {
        counterMax -= REC_COUNTER_BIG_STEP
    }

    var recommendation = if(counterMax != REC_COUNTER_DEFAULT_MAX){
        "I can't say for sure, but it seems that "
    }
    else {
        "I must say "
    }

    if(counterMax == 0.0){
        return "There is no data, so I can't tell if it's a good spot."
    }

    val percentage: Double = counter / counterMax

    when {
        percentage == 1.0 -> recommendation += "this is a perfect spot to exercise! Use this opportunity!"
        percentage >= ((2 * REC_COUNTER_BIG_STEP + REC_COUNTER_SMALL_STEP) / counterMax) -> recommendation += "this place is almost perfect! Time to exercise!"
        percentage >= ((REC_COUNTER_BIG_STEP + 2 * REC_COUNTER_SMALL_STEP) / counterMax) -> recommendation += "these are some good conditions to work out! Show me what you've got!"
        percentage >= ((REC_COUNTER_BIG_STEP + REC_COUNTER_SMALL_STEP) / counterMax) -> recommendation += "this place has above average conditions. Looks good!"
        percentage >= (REC_COUNTER_BIG_STEP / counterMax) -> recommendation += "the conditions are below average. You can make it work!"
        percentage >= (REC_COUNTER_SMALL_STEP / counterMax) -> recommendation += "this spot is pretty bad... but you can do it!"
        percentage == 0.0 -> recommendation += "the conditions are absolutely terrible here! Pick a different spot!"
    }
    if(pressure != null) {
        if (abs(perfectPressure - pressure) >= pressureCriticalOffset) {
            recommendation += if (pressure < perfectPressure) {
                " However, the barometric pressure is very low, so take extra breaks if you need to!"
            } else {
                " However, the barometric pressure is very high, so take extra breaks if you need to!"
            }
        }
    }
    if(temperature != null){
        if(abs(perfectTemperature - temperature) >= temperatureCriticalOffset){
            recommendation += if(temperature < perfectTemperature){
                " Just dress extra warm today!"
            } else {
                " Just take more water with you than usual!"
            }
        }
    }
    if(humidity != null){
        if(abs(perfectHumidity - humidity) >= humidityCriticalOffset){
            recommendation += if(humidity < perfectHumidity){
                " Oh, and it's very dry today, so take some extra water!"
            } else {
                " Oh, and watch out for the rain!"
            }
        }
    }

    return recommendation
}

@OptIn(ExperimentalPermissionsApi::class)
@Composable
fun LocationPermissions(
    navController: NavHostController,
    allLocationPermissionsState: MultiplePermissionsState
) {
    val context = LocalContext.current
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center,
        modifier = Modifier
            .padding(horizontal = 36.dp)
            .clip(RoundedCornerShape(16.dp))
            .background(Color.White)
            .fillMaxSize()
    ) {
        Text(
            modifier = Modifier.padding(top = 6.dp),
            textAlign = TextAlign.Center,
            text = "This app works 3 times better with precise location enabled"
        )
        Button(modifier = Modifier.padding(top = 12.dp), onClick = {
            allLocationPermissionsState.launchMultiplePermissionRequest()
        }) {
            Text(text = "Grant Permission")
        }
        Button(modifier = Modifier.padding(top = 12.dp), onClick = {
            if (!allLocationPermissionsState.allPermissionsGranted) {
                Toast.makeText(context, "Grant permissions first!", Toast.LENGTH_LONG).show()
                val shouldShowRationale = allLocationPermissionsState.permissions.any { !it.status.shouldShowRationale }
                if (shouldShowRationale) {
                    val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS).apply {
                        data = Uri.fromParts("package", context.packageName, null)
                    }
                    context.startActivity(intent)
                }
            } else {
                navController.navigate(Screens.Menu.route)
            }
        }) {
            Text(text = "Go to app")
        }
    }
}