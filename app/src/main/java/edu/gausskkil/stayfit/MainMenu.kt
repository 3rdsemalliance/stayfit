package edu.gausskkil.stayfit

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.material3.Button
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Tab
import androidx.compose.material3.TabRow
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import edu.gausskkil.stayfit.ui.theme.SFWhite

data class TabItem(
    val title: String
)

@OptIn(ExperimentalFoundationApi::class, ExperimentalMaterial3Api::class)
@Composable
fun MainMenu(database: AppDatabase, navController: NavController) {
    val items = listOf(
        TabItem("Outdoors"),
        TabItem("Indoors"),
        TabItem("Settings")
    )
//    database.routinesDao().purge()
//    database.exercisesDao().purge()
    val list = database.routinesDao().getIDs()
    if(list != null){
        if(list.size == 0){
            database.routinesDao().insertRoutine(Routine(database.routinesDao().getNextID(), routineName = "Example Routine", dayOfWeek = 1, time = "17:00"))
            database.exercisesDao().insertExercise(Exercise(database.exercisesDao().getNextID(), 0, "Push-ups", 1, 'S', 3, 20))
            database.exercisesDao().insertExercise(Exercise(database.exercisesDao().getNextID(), 0, "Sit-ups", 2, 'S', 3, 20))
            database.exercisesDao().insertExercise(Exercise(database.exercisesDao().getNextID(), 0, "Crunches", 3, 'S', 3, 20))
            database.exercisesDao().insertExercise(Exercise(database.exercisesDao().getNextID(), 0, "Plank Hold", 4, 'W', 3, 60))
            database.routinesDao().insertRoutine(Routine(database.routinesDao().getNextID(), routineName = "Tuesday Showdown", dayOfWeek = 2, time = "15:00"))
            database.exercisesDao().insertExercise(Exercise(database.exercisesDao().getNextID(), 1, "Superman Hold", 1, 'W', 3, 45))
            database.exercisesDao().insertExercise(Exercise(database.exercisesDao().getNextID(), 1, "Hollow Hold", 2, 'W', 3, 30))
            database.exercisesDao().insertExercise(Exercise(database.exercisesDao().getNextID(), 1, "L-sit Hold", 3, 'W', 3, 30))
            database.exercisesDao().insertExercise(Exercise(database.exercisesDao().getNextID(), 1, "Plank Hold", 4, 'W', 3, 60))
        }
    } else {
        database.routinesDao().insertRoutine(Routine(database.routinesDao().getNextID(), routineName = "Example Routine", dayOfWeek = 1, time = "17:00"))
        database.exercisesDao().insertExercise(Exercise(database.exercisesDao().getNextID(), 0, "Push-ups", 1, 'S', 3, 20))
        database.exercisesDao().insertExercise(Exercise(database.exercisesDao().getNextID(), 0, "Sit-ups", 2, 'S', 3, 20))
        database.exercisesDao().insertExercise(Exercise(database.exercisesDao().getNextID(), 0, "Crunches", 3, 'S', 3, 20))
        database.exercisesDao().insertExercise(Exercise(database.exercisesDao().getNextID(), 0, "Plank Hold", 4, 'W', 3, 60))
        database.routinesDao().insertRoutine(Routine(database.routinesDao().getNextID(), routineName = "Tuesday Showdown", dayOfWeek = 2, time = "15:00"))
        database.exercisesDao().insertExercise(Exercise(database.exercisesDao().getNextID(), 1, "Superman Hold", 1, 'W', 3, 45))
        database.exercisesDao().insertExercise(Exercise(database.exercisesDao().getNextID(), 1, "Hollow Hold", 2, 'W', 3, 30))
        database.exercisesDao().insertExercise(Exercise(database.exercisesDao().getNextID(), 1, "L-sit Hold", 3, 'W', 3, 30))
        database.exercisesDao().insertExercise(Exercise(database.exercisesDao().getNextID(), 1, "Plank Hold", 4, 'W', 3, 60))
    }
    var selectedTabIndex by remember {
        mutableIntStateOf(0)
    }
    val pagerState = rememberPagerState {
        items.size
    }
    LaunchedEffect(selectedTabIndex) {
        pagerState.animateScrollToPage(selectedTabIndex)
    }
    LaunchedEffect(pagerState, pagerState.isScrollInProgress) {
        if (!pagerState.isScrollInProgress) {
            selectedTabIndex = pagerState.currentPage
        }
    }
    Scaffold(
        topBar = {
            CenterAlignedTopAppBar(
                colors = TopAppBarDefaults.topAppBarColors(
                    containerColor = MaterialTheme.colorScheme.primaryContainer,
                    titleContentColor = MaterialTheme.colorScheme.primary,
                ),
                title = {
                    Text("Stay Fit!", color = SFWhite)
                }
            )
        },
    ) { innerPadding ->
        Column(
            modifier = Modifier.padding(innerPadding),
            verticalArrangement = Arrangement.spacedBy(4.dp)
        ) {
            TabRow(selectedTabIndex = selectedTabIndex) {
                items.forEachIndexed { index, item ->
                    Tab(
                        selected = index == selectedTabIndex,
                        onClick = {
                            selectedTabIndex = index
                        },
                        text = {
                            Text(text = item.title)
                        })
                }
            }
            HorizontalPager(
                state = pagerState,
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(1f)
            ) { index ->
                when (index) {
//                    0 -> WeatherScreen(52.455648634338125, 16.912535798866028)
                    0 -> {
                        Column(modifier = Modifier.fillMaxHeight(), horizontalAlignment = Alignment.CenterHorizontally){
                            WeatherScreen(initialLat = 52.67, initialLon = 19.03, database = database)
                            Button(modifier = Modifier
                                .height(48.dp)
                                .width(200.dp),
                            onClick = {
                                navController.navigate(Screens.Map.route)
                            }){
                                Text(text = "Sounds good, let's run!")
                            }
                        }
                    }
                    1 -> {
                        Column(modifier = Modifier.fillMaxSize()){
//                            item{
                                SensorScreen(database)
//                            }
//                            item{
                                RoutineNavigation(navController, database)
//                            }
                        }

                    }
                    2 -> SettingsScreen(database)
                }
            }
        }
    }
}
