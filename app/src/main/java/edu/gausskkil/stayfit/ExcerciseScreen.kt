package edu.gausskkil.stayfit

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign

@Composable
fun ExerciseScreen(id: Int){
    Column{
        Text(modifier = Modifier.fillMaxWidth(), text = "This displays the exercises you want to do in order", textAlign = TextAlign.Center)
        Text(text = "Displaying exercise set no. $id")
    }
}