package edu.gausskkil.stayfit.ListContentTypes

import java.util.ArrayList
import java.util.HashMap

object SettingsContent {

    /**
     * An array of settings menu options.
     */
    val ITEMS: MutableList<SettingsItem> = ArrayList()

    /**
     * A map of menu options, by ID.
     */
    val ITEM_MAP: MutableMap<String, SettingsItem> = HashMap()

    init {
        // Add some sample items.
        addItem(SettingsItem("0", "My plans"))
        addItem(SettingsItem("1", "Synchronize with remote database"))
    }

    private fun addItem(item: SettingsItem) {
        ITEMS.add(item)
        ITEM_MAP.put(item.id, item)
    }

    /**
     * An item representing a menu option.
     */
    data class SettingsItem(val id: String, val content: String) {
        override fun toString(): String = content
    }
}