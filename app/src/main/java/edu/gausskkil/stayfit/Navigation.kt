package edu.gausskkil.stayfit

import android.Manifest
import androidx.compose.runtime.Composable
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.rememberMultiplePermissionsState

sealed class Screens(val route: String) {
    data object Splash : Screens("splash_screen")
    data object Menu : Screens("main_menu")
    data object Map : Screens("map_screen")
    data object Exercise: Screens("exercise_screen")
    data object Settings: Screens("settings_screen")
    data object Permissions: Screens("location_permission")
}
@OptIn(ExperimentalPermissionsApi::class)
@Composable
fun StayFit(database: AppDatabase){
    val navController = rememberNavController()
    val allLocationPermissionState = rememberMultiplePermissionsState(
        listOf(
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
        )
    )
    NavHost(navController = navController, startDestination = Screens.Splash.route) {
        composable(route = Screens.Splash.route) {
            AnimatedSplashScreen(navController = navController, allLocationPermissionsState = allLocationPermissionState)
        }
        composable(route = Screens.Permissions.route){
            LocationPermissions(navController = navController, allLocationPermissionsState = allLocationPermissionState)
        }
        composable(route = Screens.Menu.route){
            MainMenu(database, navController = navController)
        }
        composable(
            route = Screens.Exercise.route + "/{setID}",
            arguments = listOf(
                navArgument("setID") {
                    type = NavType.IntType
                }
            )
        ){ entry ->
            entry.arguments?.getInt("setID")
                ?.let { TrainingScreen(id = it, database = database) }
        }
        composable(route = Screens.Map.route){
            MapScreen(database = database)
        }
        composable(route = Screens.Settings.route){
            SettingsScreen(database = database)
        }
    }
}
