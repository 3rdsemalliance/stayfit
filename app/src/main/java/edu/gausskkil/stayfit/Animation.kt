package edu.gausskkil.stayfit

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHost
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.airbnb.lottie.compose.LottieAnimation
import com.airbnb.lottie.compose.LottieCompositionSpec
import com.airbnb.lottie.compose.LottieConstants
import com.airbnb.lottie.compose.animateLottieCompositionAsState
import com.airbnb.lottie.compose.rememberLottieComposition
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.MultiplePermissionsState
import com.google.accompanist.permissions.rememberMultiplePermissionsState
import kotlinx.coroutines.delay

//@OptIn(ExperimentalPermissionsApi::class)
//@Composable
//fun SetUpAnimation(navController: NavHostController, database: AppDatabase) {
//    val tabList = listOf(
//        TabItem("Outdoors"),
//        TabItem("Indoors"),
//        TabItem("Settings")
//    )
//    val allLocationPermissionState = rememberMultiplePermissionsState(
//        listOf(
//            android.Manifest.permission.ACCESS_COARSE_LOCATION,
//            android.Manifest.permission.ACCESS_FINE_LOCATION
//        )
//    )
//    NavHost(
//        navController = navController,
//        startDestination = Screens.Splash.route
//    ) {
//        composable(route = Screens.Splash.route) {
//            AnimatedSplashScreen(navController = navController, allLocationPermissionState = allLocationPermissionState)
//        }
//        composable(route = Screens.Permission.route){
//            LocationPermissions(navController = navController, allLocationPermissionState = allLocationPermissionState)
//        }
//        composable(route = Screens.Home.route) {
//            MainMenu(items = tabList, database = database)
//        }
//    }
//}

@OptIn(ExperimentalPermissionsApi::class)
@Composable
fun AnimatedSplashScreen(navController: NavHostController, allLocationPermissionsState: MultiplePermissionsState){
    var startAnimation by remember { mutableStateOf(false) }
    LaunchedEffect(key1 = true) {
        startAnimation = true
        delay(4000)
        navController.popBackStack()
        if (!allLocationPermissionsState.allPermissionsGranted){
            navController.navigate(Screens.Permissions.route)
        }
        else {
            navController.navigate(Screens.Menu.route)
        }
    }
    Splash()
}

@Composable
fun Splash(){
    Surface(
        modifier = Modifier.fillMaxSize(),
        color = Color.Red
    ) {
        Box {
            AnimatedPreloader(modifier = Modifier
                .size(200.dp)
                .align(Alignment.Center))
        }
    }
}

@Composable
fun AnimatedPreloader(modifier: Modifier = Modifier) {
    val preloaderLottieComposition by rememberLottieComposition(
        LottieCompositionSpec.RawRes(
            R.raw.start_screen_animation
        )
    )
    val preloaderProgress by animateLottieCompositionAsState(
        preloaderLottieComposition,
        iterations = LottieConstants.IterateForever,
        isPlaying = true
    )
    LottieAnimation(
        composition = preloaderLottieComposition,
        progress = preloaderProgress,
        modifier = modifier
    )
}