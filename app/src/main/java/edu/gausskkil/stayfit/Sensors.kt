package edu.gausskkil.stayfit

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp


@Composable
fun SensorScreen(database: AppDatabase) {
    val context = LocalContext.current

    val pressure = remember { mutableStateOf<Float?>(null) }
    val temperature = remember { mutableStateOf<Float?>(null) }
    val humidity = remember { mutableStateOf<Float?>(null) }
    val recommendation = remember {mutableStateOf("There is no data, so I can't tell if it's a good spot.")}

    val sensorManager = remember {
        context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
    }

    val pressureSensor = remember {
        sensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE)
    }
    val temperatureSensor = remember {
        sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE)
    }
    val humiditySensor = remember {
        sensorManager.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY)
    }

    val sensorEventListener = object : SensorEventListener {
        override fun onSensorChanged(event: SensorEvent?) {
            event?.let {
                when (it.sensor.type) {
                    Sensor.TYPE_PRESSURE -> {
                        pressure.value = it.values[0]
                        recommendation.value = calculateRecommendation(pressure.value, temperature.value, humidity.value, database)
                    }
                    Sensor.TYPE_AMBIENT_TEMPERATURE -> {
                        temperature.value = it.values[0]
                        recommendation.value = calculateRecommendation(pressure.value, temperature.value, humidity.value, database)
                    }
                    Sensor.TYPE_RELATIVE_HUMIDITY -> {
                        humidity.value = it.values[0]
                        recommendation.value = calculateRecommendation(pressure.value, temperature.value, humidity.value, database)
                    }
                }
            }
        }
        override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {}
    }

    DisposableEffect(Unit) {
        pressureSensor?.let {
            sensorManager.registerListener(sensorEventListener, it, SensorManager.SENSOR_DELAY_NORMAL)
        }
        temperatureSensor?.let {
            sensorManager.registerListener(sensorEventListener, it, SensorManager.SENSOR_DELAY_NORMAL)
        }
        humiditySensor?.let {
            sensorManager.registerListener(sensorEventListener, it, SensorManager.SENSOR_DELAY_NORMAL)
        }

        onDispose {
            sensorManager.unregisterListener(sensorEventListener)
        }
    }
    Column(modifier = Modifier.padding(16.dp)) {
        Text(text = "Pressure: ${pressure.value ?: "No data"} hPa")
        Text(text = "Temperature: ${temperature.value ?: "No data"} °C")
        Text(text = "Humidity: ${humidity.value ?: "No data"} %")
        Text(text = recommendation.value, modifier = Modifier.fillMaxWidth(), textAlign = TextAlign.Justify, fontSize = 20.sp)
    }
}